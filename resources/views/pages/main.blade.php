@extends('templates.container')
@section('title')@parent Feedback @endsection
@section('container')

	<div class="starter-template">
		<h1>Test work - feedback</h1>
		<hr>
		@if(!empty($userFeedback))
			<h3>Post has already been added</h3>
			<div class="row">
        <div class="col-md-2"><strong>ID</strong></div>
        <div class="col-md-10">{{$userFeedback->id}}</div>
      </div>
      <div class="row">
        <div class="col-md-2"><strong>Email</strong></div>
        <div class="col-md-10">{{$userFeedback->email}}</div>
      </div>
      <div class="row">
        <div class="col-md-2"><strong>Text</strong></div>
        <div class="col-md-10">{{$userFeedback->text}}</div>
      </div>
			<hr>
		@endif
		@if(!empty($userFeedback))
			<h3>Replace the post</h3>
		@else
			<h3>New post</h3>
		@endif
		<form method="POST">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<div class="form-group">
				<label for="email">Email:</label>
				<input type="email" class="form-control" id="email" name="email" value="{{$email or ''}}" placeholder="Enter email" maxlength="250" required>
			</div>
			<div class="form-group">
				<label for="text">Text:</label>
				<textarea class="form-control" id="text" name="text" placeholder="Enter text" maxlength="5000">{{$text or ''}}</textarea>
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-default">Submit</button>
			</div>
		</form>	

		@include('sections.flashAlerts')

	</div>

@endsection