<!DOCTYPE html>
<html>
	<head>
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<title>@yield('title', '-no title-')</title>
		@yield('header')
	</head>
	<body>@yield('body')@yield('bottomScript')</body>
</html>