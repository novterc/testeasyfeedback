<div class="flash-message">
	@foreach (['danger', 'warning', 'success', 'info'] as $msgType)
		@if(Session::has('alert-' . $msgType))
			@if(is_array(Session::get('alert-' . $msgType)))
				@foreach( Session::get('alert-' . $msgType) as $msg )
					@include('sections.flashAlertItem', ['type' => 'danger', 'msg' => $msg[0] ])
				@endforeach
			@else
				@include('sections.flashAlertItem', ['type' => $msgType, 'msg' => Session::get('alert-' . $msgType) ])	
			@endif
		@endif
	@endforeach
</div>