<?php

namespace App\Http\Controllers;

use App\Models\Feedback;
use Illuminate\Http\Request;
use Validator;

class FeedbacksController extends Controller {

	public function index(Request $request)
	{
		$sessionId = $request->session()->getId();
		$userFeedback = Feedback::getBySessionId($sessionId)->first();

		return view('pages.main')
				->with('userFeedback', $userFeedback);
	}

	public function add(Request $request)
	{
		$validator = Validator::make($request->all(), [
            'email' => 'required|email|max:255',
            'text' => 'max:5000'
        ]);

        if ($validator->fails()) {
        	$errorsMessages = $validator->errors()->getMessages();
        	$request->session()->flash('alert-danger', $errorsMessages);
        	
            return view('pages.main')
            		->with($request->all());
        }

        $feedbackAttrs = $request->all();
		$sessionId = $request->session()->getId();
        Feedback::createOnceBySessionId($sessionId, $feedbackAttrs);
        $request->session()->flash('alert-success', 'Post was added!');

        return redirect()->action('FeedbacksController@index');
	}

}