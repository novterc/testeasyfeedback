<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model {

	protected $table = 'feedbacks';
    protected $guarded = ['id'];

    public function scopeGetBySessionId($query, $sessionId)
    {
    	return $query->where('session_id', $sessionId);
    }

    public static function createOnceBySessionId($sessionId, $attrs)
    {
    	static::getBySessionId($sessionId)->delete();
    	$attrs['session_id'] = $sessionId;
    	static::create($attrs);
    }
}